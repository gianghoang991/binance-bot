import {
  CandlestickWebSocket,
} from "./modules/binance/binance";
import { getConfig } from "./libs/get_config";
import { RegisterBot } from "./modules/telebot/telegram";
import { Constants, periods } from "./modules/constants";
import express from "express";
import * as fs from "fs";
import { Models } from "./libs/db/models";
import {
  autoTrade,
  initAutoTradeWorker,
} from "./modules/auto-trade/job-auato-trade";
import { connectRedis } from "./libs/cache_helper";

async function main() {
  await connectRedis();

  var app = express();
  var cors = require("cors");
  app.use(express.json());
  app.use(cors());
  const jwt = require("jsonwebtoken");
  // Handling post request
  app.post("/login", async (req, res, next) => {
    let { username, password } = req.body;
    let appsettings = getConfig("app");
    let adminUsername = appsettings["username"];
    let adminPassword = appsettings["password"];

    if (adminUsername != username || adminPassword != password) {
      const error = Error("Wrong details please check at once");
      return next(error);
    }
    let token;
    try {
      //Creating jwt token
      token = jwt.sign(
        { userId: 1, username: adminUsername },
        appsettings["secret"],
        { expiresIn: "1h" }
      );
    } catch (err) {
      console.log(err);
      const error = new Error("Error! Something went wrong.");
      return next(error);
    }

    res.status(200).json({
      success: true,
      data: {
        username: adminUsername,
        token: token,
      },
    });
  });

  app.get("/", (request, response) => {
    response.send("hello");
  });

  app.get("/config", async (request, response) => {
    try {
      const config = await Models.config_settings.findFirst({
        where: {
          id: 1,
        },
      });
      response.send({
        TOP_BOTTON_RATIO: config?.top_bottom_ratio,
        REBOUND_FACTOR: config?.rebound_factor,
        MAX_HIDDEN: config?.max_hidden,
        MIN_HIDDEN: config?.min_hidden,
        MAX: config?.max,
        MIN: config?.min,
        TP: {
          TP1: config?.tp1,
          TP2: config?.tp2,
          TP3: config?.tp3,
          TP4: config?.tp4,
          TP5: config?.tp5,
          TP6: config?.tp6,
          TP7: config?.tp7,
          TP8: config?.tp8,
          TP9: config?.tp9,
          TP10: config?.tp10,
          TP11: config?.tp11,
        },
        SL: {
          SL_RATIO: config?.sl_ratio,
          SL_TOP: 0,
          SL_BOTTON: 0,
        },
        ENTRY: {
          ENTRY1: config?.entry1,
          ENTRY2: config?.entry2,
          ENTRY3: config?.entry3,
        },
        CONFIG_H4: config?.config_h4_top_bottom_ratio,
        CONFIG_1D: config?.config_1d_top_bottom_ratio,
      });
    } catch (e) {
      console.log(e);
    }
  });

  app.get("/last-signal", (request, response) => {
    const rawdata = fs.readFileSync("./last-signal.json", "utf8");
    const data = JSON.parse(rawdata);
    response.send(data);
  });

  // app.get('/auto-trade', async (request, response) => {
  //   try {
  //     const data = request.body
  //     await autoTrade(data.users, data.signal)
  //     response.send(data);
  //   } catch (e) {
  //     console.log(e)
  //   }
  // });

  app.put("/config", async (request, response) => {
    try {
      console.log(request.body);

      let data: any = {};
      for (let item of request.body) {
        data[
          `${item.key
            .toLowerCase()
            .replace("tp:", "")
            .replace("sl:", "")
            .replace("entry:", "")}`
        ] = item.value;
      }
      console.log(data);

      const res = await Models.config_settings.update({
        where: {
          id: 1,
        },
        data: {
          top_bottom_ratio: Number(data.top_botton_ratio),
          rebound_factor: Number(data.rebound_factor),
          config_h4_top_bottom_ratio: Number(data?.config_h4),
          config_1d_top_bottom_ratio: Number(data?.config_1d),
          max: Number(data.max),
          min: Number(data.min),
          max_hidden: Number(data.max_hidden),
          min_hidden: Number(data.min_hidden),
          tp1: Number(data.tp1),
          tp2: Number(data.tp2),
          tp3: Number(data.tp3),
          tp4: Number(data.tp4),
          tp5: Number(data.tp5),
          tp6: Number(data.tp6),
          tp7: Number(data.tp7),
          tp8: Number(data.tp8),
          tp9: Number(data.tp9),
          tp10: Number(data.tp10),
          tp11: Number(data.tp11),
          sl_ratio: Number(data.sl_ratio),
          entry1: Number(data.entry1),
          entry2: Number(data.entry2),
          entry3: Number(data.entry3),
        },
      });

      response.status(200).json({
        data: res,
        success: true,
      });
    } catch (e) {
      response.status(500).send(e.message);
    }
  });

  app.listen(getConfig("APP_PORT"));

  // await connectRedis();
  // const server = http.createServer(httpServer.callback());
  // createWsServer(server);
  // server.listen(getConfig("APP_PORT"));

  const tokens = [
    {
      token: Constants.TELEGRAM.TOKEN,
      chatId: Constants.TELEGRAM.CHAT_ID,
    },
  ];
  // await FutureChart("BTCUSDT", periods.M15);
  const bot = await RegisterBot(tokens);
  CandlestickWebSocket(bot, "BTCUSDT", periods.M15, {
    token: Constants.TELEGRAM.TOKEN,
    chatId: Constants.TELEGRAM.CHAT_ID,
  });

  console.log(
    `App "${getConfig("app.name") + " dev"}" run on ${getConfig("APP_PORT")}`
  );
}

void main();
