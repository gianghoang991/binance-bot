import { exec } from "child_process";
import { v1 as getUuid } from "uuid";
import { MyError } from "./errors";
import { format as dateFormat } from "date-fns";
import rp from "request-promise";

export const helper = {
  async wait(ms: number): Promise<void> {
    return new Promise((resolve: any) => setTimeout(resolve, ms));
  },
  getFolderDate(mode: string = "date"): string {
    if (mode === "date") {
      return `${dateFormat(new Date(), "yyyy_LL_dd")}`;
    }
    if (mode === "time") {
      return `${dateFormat(new Date(), "HH_mm")}`;
    }
    if (mode === "timeDetail") {
      return `${dateFormat(new Date(), "HH_mm_ss")}`;
    }
    if (mode === "full") {
      return `${dateFormat(new Date(), "yyyy_LL_dd_HH_mm_ss")}`;
    }
    return `${dateFormat(new Date(), "yyyy_LL_dd")}`;
  },
  async makeRequest(options: rp.Options) {
    return rp({
      ...options,
    });
  },
  async runCmd(cmd: string, options: any = {}): Promise<string> {
    const now = new Date();
    return new Promise((resolve: any, reject: any) => {
      exec(cmd, options, async (err, stdout, stderr) => {
        if (err) {
          if (options?.timeout) {
            const duration = new Date().getTime() - now.getTime();
            if (duration > options.timeout) {
              return reject(MyError.ServerError(`Timeout after ${duration}ms`));
            }
          }
          return reject(MyError.ServerError(`cant start '${cmd}' ${err}`, err));
        }
        return resolve(stdout);
      });
    });
  },
  getNewUuid() {
    return getUuid();
  },
};
