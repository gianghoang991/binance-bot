import Koa, { BaseContext } from "koa";
import fs from "fs";
import path from "path";
import { helper } from "./helper";
import { getConfig } from "./get_config";

export const ApiSuccessHandler = {
  sendJson(ctx: BaseContext, message = "", data = {}): void {
    ctx.status = 200;
    ctx.body = {
      message,
      data,
    };
  },
};

export const requestHelper = {
  /* Giúp parse ctx để lấy file để thao tác */
  async parseFileUpload(ctx: Koa.Context) {
    const rqFiles = ctx.request.files || {};
    const out: any = {};
    const entries = Object.entries(rqFiles);
    for (const [key, value] of entries) {
      const files: any = value || [];
      out[key] = await Promise.all(
        files.map(async (file: any) => {
          const fileName = file.filename;
          const tmp = getConfig("DIR_TMP");
          const folder = path.join(tmp, helper.getFolderDate());
          await fs.promises.mkdir(folder, {
            recursive: true,
          });
          const fileExtension = file.originalname.split(".").pop();
          await fs.promises.rename(
            file.path,
            path.join(folder, `${fileName}.${fileExtension}`)
          );
          const fileSize = file.size;
          const fileOriginalName = file.originalname;
          const fileEncoding = file.encoding;
          const fileMime = file.mimetype;
          return {
            fileName,
            filePath: path.join(folder, `${fileName}.${fileExtension}`),
            fileSize,
            fileOriginalName,
            fileEncoding,
            fileMime,
            fileExtension,
          };
        })
      );
    }

    return out;
  },
};
