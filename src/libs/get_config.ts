import config from "config";

export function getConfig(key: string, defaultValue: any = null) {
  let value = config.get(key);
  if (typeof value === "undefined") {
    return defaultValue;
  }
  return value;
}
