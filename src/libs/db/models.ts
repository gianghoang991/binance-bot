import { PrismaClient } from "@prisma/client";

export const prisma = new PrismaClient();
export const Models = {
  User: prisma.users,
  UserCopyTrade: prisma.user_copy_trades,
  UserAutoTrade: prisma.user_bots,
  order: prisma.orders,
  config_settings: prisma.config_settings,
  history_signal: prisma.signal_histories,
  follow_user: prisma.user_follows,
};
