import { connectRedis } from "./libs/cache_helper";
import { initAutoTradeWorker } from "./modules/auto-trade/job-auato-trade";

async function main() {
  await initAutoTradeWorker();
  // await LastPriceWebSocket();
}

void main();
