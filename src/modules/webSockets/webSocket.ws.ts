import WebSocket from "ws";
import { helper } from "../../libs/helper";

let connections: any[] = [];

export function sendToToRoom(
  room: string,
  status: string,
  data: any,
  message: string
) {
  connections.forEach((connect: any) => {
    if (connect.room.some((item: string) => item === room)) {
      connect.ws.send(JSON.stringify({ room, data, status, message }));
    }
  });
}

export function sendAll(data: any) {
  console.log("connects", connections.length);
  connections.forEach((connect: any) => {
    connect.ws.send(JSON.stringify(data));
  });
}
// to
function getWsController(): WebSocket.Server {
  const wss = new WebSocket.Server({ noServer: true });

  wss.on("connection", async function connection(ws, req) {
    console.log("connecting");
    const uid = helper.getNewUuid();
    connections.push({ uid, ws, room: [] });
    ws.on("message", async (message: string) => {
      console.log("received: %s", message.toString());
      try {
        const body = JSON.parse(message.toString());

        if (body.action === "join" && body?.data?.room) {
          const room = body?.data?.room;
          const index = connections.findIndex((x) => x.uid === uid);
          if (index > -1) {
            connections[index].room.push(room);
          }
          // sendToToRoom(room, "CONNECTED", {},  "connected to room");
          //todo leave event
        }
      } catch (e) {
        console.log("cant parse message");
      }
      ws.send(message);
    });
    ws.on("close", function close() {
      connections = connections.filter((x) => x.uid !== uid);
      console.log("disconnected");
    });
  });
  wss.on("error", console.log);
  return wss;
}

const GeneralWsController = getWsController();
export default GeneralWsController;
