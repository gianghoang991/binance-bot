export namespace TeleBot {
  type TeleBot = {
    flg_tele_bot: boolean;
  };
  type Trend = {
    UP: "UP";
    DOWN: "DOWN";
  };
  type BotType = {
    token: string;
    chatId: string;
  };
}
