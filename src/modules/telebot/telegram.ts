import { Config } from "../constants";
import { Constants, periods } from "../constants";
import {
  CandlestickData,
  CandlestickWebSocket,
  MiniTicker,
} from "../binance/binance";
import { TelegramBotHelper } from "./helper";
import { TeleBot } from "./type";
import * as fs from "fs";
import { Models } from "../../libs/db/models";

const TelegramBot = require("node-telegram-bot-api");

export const RegisterBot = async (tokens: TeleBot.BotType[]): Promise<any> => {
  console.log(tokens);
  tokens.forEach(async (TeleBot: TeleBot.BotType) => {
    const bot = new TelegramBot(TeleBot.token, { polling: true });
    await CandlestickWebSocket(bot, "BTCUSDT", periods.M15, TeleBot);
    bot.onText(/\/start/, (msg: any) => {
      bot.sendMessage(msg.chat.id, "start", {
        reply_markup: {
          keyboard: [["/signal", "😍"]],
        },
      });
    });
    bot.onText(/\/clear/, (msg: any) => {
      bot.sendMessage(msg.chat.id, "clear");
    });
    // // Matches "/echo [whatever]"
    bot.onText(/\/signal/, async (msg: any, match: any) => {
      try {
        // 'msg' is the received Message from Telegram
        // 'match' is the result of executing the regexp above on the text content
        // of the message
        const chatId = msg.chat.id;
        console.log(chatId);
        const text = await TelegramBotHelper.getDataBuySymbol(periods.M15, bot);
        bot.sendMessage(chatId, text);
      } catch (e) {
        console.log(e);
      }
    });
  });
};
// Create a bot that uses 'polling' to fetch new updates
