import { Models } from "../../libs/db/models";
import { helper } from "../../libs/helper";
import { FutureChart, MiniTicker } from "../binance/binance";
import { BinanceType } from "../binance/type";
import { Constants, periods, Trend } from "../constants";
import { Config } from "../constants";
import { CandlestickData } from "../binance/binance";
import * as fs from "fs";
import { config_settings } from "@prisma/client";
import { any, length } from "ramda";
import { TeleBot } from "./type";
import { redisCacheClient } from "../../libs/cache_helper";

enum TradeType {
  LONG = "LONG",
  SHORT = "SHORT",
  NONE = "NONE",
}

const logChatId = 2133891474;

function compare_percentage(params: { bigger: number; smaller: number }) {
  const { bigger, smaller } = params;
  return (bigger / smaller - 1) * 100;
}

async function logToTelegram(params: { bot: any; text: string }) {
  return new Promise((res: any, rej: any) => {
    const { bot, text } = params;

    if (bot && text) {
      res(bot.sendMessage(logChatId, text));
    }
  });
}

async function getCandles(
  symbol: string,
  period: periods,
  limit: number = Config.compareCandle
): Promise<BinanceType.BinanceTick[]> {
  let data = await helper.makeRequest({
    url: `https://www.binance.com/fapi/v1/continuousKlines?limit=${limit}&pair=${symbol}&contractType=PERPETUAL&interval=${period}`,
  });
  data = JSON.parse(data);
  return data.map((tick: any) => {
    const [
      time,
      open,
      high,
      low,
      close,
      volume,
      closeTime,
      assetVolume,
      trades,
      buyBaseVolume,
      buyAssetVolume,
      ignored,
    ] = tick;
    const tickData: BinanceType.BinanceTick = {
      time: new Date(time),
      open: Number(open),
      high: Number(high),
      low: Number(low),
      close: Number(close),
      volume: Number(volume),
      closeTime: new Date(closeTime),
      assetVolume: Number(assetVolume),
      trades: parseInt(trades),
      buyBaseVolume: Number(buyBaseVolume),
      buyAssetVolume: Number(buyAssetVolume),
      ignored: Number(ignored),
    };
    return tickData;
  });
}

async function getSettings() {
  return await Models.config_settings.findFirst({
    where: {
      id: 1,
    },
  });
}

function convertTime(date: Date) {
  return new Date(date).toLocaleString("en", {
    timeZone: "Asia/Ho_Chi_Minh",
  });
}

async function message(dataConfig: config_settings, trend: TradeType) {
  const is_uptrend = trend === TradeType.LONG ? true : false;
  const formatNumber = 2;
  const entry1 = is_uptrend
    ? dataConfig.min * ((100 + dataConfig.entry1) / 100)
    : dataConfig.max * ((100 - dataConfig.entry1) / 100);
  const entry2 = is_uptrend
    ? dataConfig.min * ((100 + dataConfig.entry2) / 100)
    : dataConfig.max * ((100 - dataConfig.entry2) / 100);
  const entry3 = is_uptrend
    ? dataConfig.min * ((100 + dataConfig.entry3) / 100)
    : dataConfig.max * ((100 - dataConfig.entry3) / 100);
  const sl = is_uptrend
    ? dataConfig.min * ((100 - dataConfig.sl_ratio) / 100)
    : dataConfig.max * ((100 + dataConfig.sl_ratio) / 100);
  const tp1 = is_uptrend
    ? dataConfig.min * ((100 + dataConfig.tp1) / 100)
    : dataConfig.max * ((100 - dataConfig.tp1) / 100);
  const tp2 = is_uptrend
    ? dataConfig.min * ((100 + dataConfig.tp2) / 100)
    : dataConfig.max * ((100 - dataConfig.tp2) / 100);
  const tp3 = is_uptrend
    ? dataConfig.min * ((100 + dataConfig.tp3) / 100)
    : dataConfig.max * ((100 - dataConfig.tp3) / 100);
  const tp4 = is_uptrend
    ? dataConfig.min * ((100 + dataConfig.tp4) / 100)
    : dataConfig.max * ((100 - dataConfig.tp4) / 100);
  const tp5 = is_uptrend
    ? dataConfig.min * ((100 + dataConfig.tp5) / 100)
    : dataConfig.max * ((100 - dataConfig.tp5) / 100);
  const tp6 = is_uptrend
    ? dataConfig.min * ((100 + dataConfig.tp6) / 100)
    : dataConfig.max * ((100 - dataConfig.tp6) / 100);
  const tp7 = is_uptrend
    ? dataConfig.min * ((100 + dataConfig.tp7) / 100)
    : dataConfig.max * ((100 - dataConfig.tp7) / 100);
  const tp8 = is_uptrend
    ? dataConfig.min * ((100 + dataConfig.tp8) / 100)
    : dataConfig.max * ((100 - dataConfig.tp8) / 100);
  const tp9 = is_uptrend
    ? dataConfig.min * ((100 + dataConfig.tp9) / 100)
    : dataConfig.max * ((100 - dataConfig.tp9) / 100);
  const tp10 = is_uptrend
    ? dataConfig.min * ((100 + dataConfig.tp10) / 100)
    : dataConfig.max * ((100 - dataConfig.tp10) / 100);
  const tp11 = is_uptrend
    ? dataConfig.min * ((100 + dataConfig.tp11) / 100)
    : dataConfig.max * ((100 - dataConfig.tp11) / 100);

  const mesages =
    `🤖 BOT - ${dataConfig.symbol.toUpperCase()}\n` +
    `Symbol: ${dataConfig.symbol.toUpperCase()}\n\n` +
    `    —————🌟—————\n` +
    `「1. 🌟 Trend: ${is_uptrend ? TradeType.LONG : TradeType.SHORT}` +
    `-\n` +
    `↗️ Đỉnh: ${dataConfig.max} \n\n` +
    `↘️ Đáy: ${dataConfig.min}\n` +
    `-\n` +
    `👉 Entry 1: ${entry1.toFixed(formatNumber)}\n` +
    `👉 Entry 2: ${entry2.toFixed(formatNumber)}\n` +
    `👉 Entry 3: ${entry3.toFixed(formatNumber)}\n\n` +
    `   —————🌟—————\n` +
    `「2. 🌟 Stop-loss:\n` +
    `-\n` +
    `👉 Stop-loss: ${sl.toFixed(formatNumber)}\n\n` +
    `   —————🌟—————\n` +
    `「3. Take-profit:\n` +
    `-\n` +
    `👉 TP1: ${tp1.toFixed(formatNumber)}\n` +
    `👉 TP2: ${tp2.toFixed(formatNumber)}\n` +
    `👉 TP3: ${tp3.toFixed(formatNumber)}\n` +
    `👉 TP4: ${tp4.toFixed(formatNumber)}\n` +
    `👉 TP5: ${tp5.toFixed(formatNumber)}\n` +
    `👉 TP6: ${tp6.toFixed(formatNumber)}\n` +
    `👉 TP7: ${tp7.toFixed(formatNumber)}\n` +
    `👉 TP8: ${tp8.toFixed(formatNumber)}\n` +
    `👉 TP9: ${tp9.toFixed(formatNumber)}\n` +
    `👉 TP10: ${tp10.toFixed(formatNumber)}\n` +
    `👉 TP11: ${tp11.toFixed(formatNumber)}\n`;

  const exitSignal = await Models.history_signal.findMany({
    where: {
      message: mesages,
      entry_1: String(entry1.toFixed(formatNumber)),
      entry_2: String(entry2.toFixed(formatNumber)),
      entry_3: String(entry3.toFixed(formatNumber)),
      tp1: String(tp1.toFixed(formatNumber)),
      tp2: String(tp2.toFixed(formatNumber)),
      tp3: String(tp3.toFixed(formatNumber)),
      tp4: String(tp4.toFixed(formatNumber)),
      tp5: String(tp5.toFixed(formatNumber)),
      tp6: String(tp6.toFixed(formatNumber)),
      tp7: String(tp7.toFixed(formatNumber)),
      tp8: String(tp8.toFixed(formatNumber)),
      tp9: String(tp9.toFixed(formatNumber)),
      tp10: String(tp10.toFixed(formatNumber)),
      tp11: String(tp11.toFixed(formatNumber)),
      stop_loss: String(sl.toFixed(formatNumber)),
    },
    take: 1,
    orderBy: {
      created_at: "desc",
    },
  });

  if (exitSignal.length === 0) {
    const signal = is_uptrend ? TradeType.LONG : TradeType.SHORT;

    console.log("signal: ", signal);

    const data = await Models.history_signal.create({
      data: {
        base: `${is_uptrend ? TradeType.LONG : TradeType.SHORT}`,
        bot_id: dataConfig.id,
        symbol: dataConfig.symbol,
        message: mesages,
        entry_price: String(entry1.toFixed(formatNumber)),
        entry_1: String(entry1.toFixed(formatNumber)),
        entry_2: String(entry2.toFixed(formatNumber)),
        entry_3: String(entry3.toFixed(formatNumber)),
        tp1: String(tp1.toFixed(formatNumber)),
        tp2: String(tp2.toFixed(formatNumber)),
        tp3: String(tp3.toFixed(formatNumber)),
        tp4: String(tp4.toFixed(formatNumber)),
        tp5: String(tp5.toFixed(formatNumber)),
        tp6: String(tp6.toFixed(formatNumber)),
        tp7: String(tp7.toFixed(formatNumber)),
        tp8: String(tp8.toFixed(formatNumber)),
        tp9: String(tp9.toFixed(formatNumber)),
        tp10: String(tp10.toFixed(formatNumber)),
        tp11: String(tp11.toFixed(formatNumber)),
        stop_loss: String(sl.toFixed(formatNumber)),
      },
    });
    await redisCacheClient.publish("signal", JSON.stringify(data));
    return mesages;
  }
  return null;
}

let index = 450;

export const TelegramBotHelper = {
  // Helper function to check the difference between two numbers in percentage
  async getDataBuySymbol(
    period: periods = periods.M15,
    bot?: any,
    chatId?: string
  ): Promise<any> {
    try {
      index += 1;
      await logToTelegram({
        bot,
        text: "--------------------------------",
      });
      let dataConfig = await getSettings();

      const arrayCompareCandleCandleNear = (
        await getCandles(dataConfig.symbol, period, 500)
      ).reverse();

      // test
      // let arrayCompareCandleCandleNear = await getCandles(
      //   dataConfig.symbol,
      //   period,
      //   500
      // );
      arrayCompareCandleCandleNear.pop();

      // arrayCompareCandleCandleNear = arrayCompareCandleCandleNear.slice(400, index).reverse()
      // arrayCompareCandleCandleNear = arrayCompareCandleCandleNear.reverse();

      const currentCloseCandle = arrayCompareCandleCandleNear[0];
      await logToTelegram({
        bot,
        text: `thời gian mở: ${convertTime(
          currentCloseCandle.time
        )}, giá đóng cửa: ${currentCloseCandle.close}`,
      });

      await logToTelegram({
        bot,
        text: `khung giá: ${currentCloseCandle.high}-${currentCloseCandle.low}`,
      });

      let positionMaxNear = -1;
      let positionMinNear = -1;
      let trend = TradeType.NONE;

      const arrayTop = [];
      const arrayBottom = [];

      // xác định tọa độ đỉnh đáy
      for (const [index, iterator] of arrayCompareCandleCandleNear.entries()) {
        if (iterator.low === dataConfig.min) {
          arrayBottom.push(index);
        }
        if (iterator.high === dataConfig.max) {
          arrayTop.push(index);
        }
      }

      // lấy tọa độ đỉnh và đáy
      if (arrayTop.length !== 0) {
        positionMaxNear = arrayTop[0];
      }

      if (arrayBottom.length !== 0) {
        positionMinNear = arrayBottom[0];
      }

      let flagLow = currentCloseCandle.low;
      const low_points = [flagLow];
      for (const [index, iterator] of arrayCompareCandleCandleNear.entries()) {
        if (iterator.low <= flagLow) {
          flagLow = iterator.close;
          low_points.push(iterator.low);
        } else {
          break;
        }
      }
      let flagHigh = currentCloseCandle.high;
      const high_points = [flagHigh];
      for (const [index, iterator] of arrayCompareCandleCandleNear.entries()) {
        if (iterator.high >= flagHigh) {
          flagHigh = iterator.close;
          high_points.push(iterator.high);
        } else {
          break;
        }
      }

      const iter_max = Math.max.apply(Math, high_points);
      const iter_min = Math.min.apply(Math, low_points);

      await logToTelegram({
        bot,
        text: `high_points: ${high_points}, iter_max: ${iter_max}`,
      });

      await logToTelegram({
        bot,
        text: `low_points: ${low_points}, iter_min: ${iter_min}`,
      });

      await logToTelegram({
        bot,
        text: `positionMaxNear: ${positionMaxNear}`,
      });

      await logToTelegram({
        bot,
        text: `positionMinNear: ${positionMinNear}`,
      });

      // xác định trend
      // nếu đỉnh gần thì Short
      if (positionMaxNear < positionMinNear) {
        trend = TradeType.SHORT;
      }

      // nếu đáy gần thì long
      if (positionMinNear < positionMaxNear) {
        trend = TradeType.LONG;
      }

      // nếu đáy đỉnh cùng 1 cây nến
      if (positionMinNear === positionMaxNear) {
        trend = TradeType.NONE;
      }

      // kill long short
      if (trend === TradeType.NONE) {
        if (currentCloseCandle.open < currentCloseCandle.close) {
          trend = TradeType.LONG;
        }

        if (currentCloseCandle.open > currentCloseCandle.close) {
          trend = TradeType.SHORT;
        }
      }

      await logToTelegram({
        bot,
        text: `Trend: ${trend}`,
      });

      const top_bottom_ratio = Math.abs(
        compare_percentage({
          bigger: dataConfig.max,
          smaller: dataConfig.min,
        })
      );

      await logToTelegram({
        bot,
        text: `Tỉ lệ đáy đỉnh đạt: ${top_bottom_ratio.toFixed(2)}`,
      });

      await logToTelegram({
        bot,
        text: `min: ${dataConfig.min}, max: ${dataConfig.max}`,
      });

      if (iter_max > dataConfig.max) {
        dataConfig = await Models.config_settings.update({
          where: {
            id: dataConfig.id,
          },
          data: {
            max: iter_max,
          },
        });
        await logToTelegram({
          bot,
          text: `Cập nhật đỉnh mới: ${iter_max} \n`,
        });
      }

      if (iter_min < dataConfig.min) {
        dataConfig = await Models.config_settings.update({
          where: {
            id: dataConfig.id,
          },
          data: {
            min: iter_min,
          },
        });
        await logToTelegram({
          bot,
          text: `Cập nhật đáy mới: ${dataConfig.min} \n`,
        });
      }

      if (trend === TradeType.LONG) {
        // cập nhật đỉnh ẩn
        await logToTelegram({
          bot,
          text: `cập nhật min_hidden (${iter_min}-${dataConfig.min_hidden}): ${
            iter_min >= dataConfig.min_hidden
          }`,
        });
        if (iter_min < dataConfig.min_hidden) {
          dataConfig = await Models.config_settings.update({
            where: {
              id: dataConfig.id,
            },
            data: {
              min_hidden: iter_min,
            },
          });
        }

        if (top_bottom_ratio >= dataConfig.top_bottom_ratio) {
          const iter_top_bottom_ratio = Math.abs(
            compare_percentage({
              bigger: iter_max,
              smaller: dataConfig.min,
            })
          );
          await logToTelegram({
            bot,
            text: `Tỉ lệ iter_top_bottom_ratio: ${iter_top_bottom_ratio.toFixed(
              2
            )}`,
          });
          const lastMax = dataConfig.max;
          if (iter_top_bottom_ratio >= dataConfig.top_bottom_ratio) {
            dataConfig = await Models.config_settings.update({
              where: {
                id: dataConfig.id,
              },
              data: {
                max: iter_max,
              },
            });
          }
          // khi bật ngược theo tỉ lệ
          const rebound_factor_ratiro = compare_percentage({
            bigger: currentCloseCandle.close,
            smaller: dataConfig.min_hidden,
          });
          await logToTelegram({
            bot,
            text: `bật ngược với đáy (${
              dataConfig.min_hidden
            }): ${rebound_factor_ratiro.toFixed(2)}`,
          });
          if (rebound_factor_ratiro >= dataConfig.rebound_factor) {
            await logToTelegram({
              bot,
              text: `Đủ điều kiện bắn lệnh Long`,
            });

            dataConfig = await Models.config_settings.update({
              where: {
                id: dataConfig.id,
              },
              data: {
                min: dataConfig.min_hidden,
                max_hidden: dataConfig.min_hidden,
              },
            });
            dataConfig.max = lastMax;
            const text = await message(dataConfig, trend);
            await logToTelegram({
              bot,
              text: text,
            });

            // bắn vào redis

            return text;
          }
        }
      }
      if (trend === TradeType.SHORT) {
        // cập nhật đáy ẩn
        await logToTelegram({
          bot,
          text: `cập nhật max_hidden (${iter_max}-${dataConfig.max_hidden}): ${
            iter_max <= dataConfig.max_hidden
          }`,
        });

        if (iter_max > dataConfig.max_hidden) {
          dataConfig = await Models.config_settings.update({
            where: {
              id: dataConfig.id,
            },
            data: {
              max_hidden: iter_max,
            },
          });
        }

        if (top_bottom_ratio >= dataConfig.top_bottom_ratio) {
          const iter_top_bottom_ratio = Math.abs(
            compare_percentage({
              bigger: dataConfig.max,
              smaller: iter_min,
            })
          );
          await logToTelegram({
            bot,
            text: `Tỉ lệ iter_top_bottom_ratio: ${iter_top_bottom_ratio.toFixed(
              2
            )}`,
          });
          const lastMin = dataConfig.min;
          if (iter_top_bottom_ratio >= dataConfig.top_bottom_ratio) {
            dataConfig = await Models.config_settings.update({
              where: {
                id: dataConfig.id,
              },
              data: {
                min: iter_min,
              },
            });
          }
          // cập nhật đáy ẩn
          const rebound_factor_ratiro = compare_percentage({
            bigger: dataConfig.max_hidden,
            smaller: currentCloseCandle.close,
          });
          await logToTelegram({
            bot,
            text: `bật ngược với đỉnh (${
              dataConfig.max_hidden
            }): ${rebound_factor_ratiro.toFixed(2)}`,
          });
          if (rebound_factor_ratiro >= dataConfig.rebound_factor) {
            await logToTelegram({
              bot,
              text: `Đủ điều kiện bắn lệnh Short`,
            });
            dataConfig = await Models.config_settings.update({
              where: {
                id: dataConfig.id,
              },
              data: {
                max: dataConfig.max_hidden,
                min_hidden: dataConfig.max_hidden,
              },
            });

            dataConfig.min = lastMin;
            const text = await message(dataConfig, trend);
            await logToTelegram({
              bot,
              text: text,
            });

            // bắn vào redis

            return text;
          }
        }
      }

      return null;
    } catch (e) {
      console.log(e);
    }
  },
  async botH4Config(symbol: string = "BTCUSDT", period: periods = periods.H4) {
    try {
      let dataConfig = await getSettings();

      // const arrayCompareCandleCandleNear = (await getCandles(symbol, periods.H4)).reverse()
      // List of all high points
      const high_points = [];
      // List of all low points
      const low_points = [];
      const prices = [];
      const limit = 5;

      const arrayCompareCandleCandleNear = (
        await getCandles(dataConfig.symbol, periods.H4, limit)
      ).reverse();
      // For all respones
      for (let res of Object.values(arrayCompareCandleCandleNear)) {
        // Append the information
        high_points.push(res.high);
        low_points.push(res.low);
        prices.push(res.close);
      }

      // Update the iter variables
      const maxOfRangeCandle = Math.max.apply(Math, high_points);
      const minOfRangeCandle = Math.min.apply(Math, low_points);

      if (
        compare_percentage({
          bigger: maxOfRangeCandle,
          smaller: minOfRangeCandle,
        }) >= dataConfig.config_1d_top_bottom_ratio
      ) {
        return `H4 thỏa mãn điều kiện  ${compare_percentage({
          bigger: maxOfRangeCandle,
          smaller: minOfRangeCandle,
        }).toFixed(2)}%`;
      }

      return "";
    } catch (e) {
      console.log(e);
    }
  },
  async bot1DConfig(symbol: string = "BTCUSDT", period: periods = periods.D1) {
    try {
      let dataConfig = await getSettings();

      // const arrayCompareCandleCandleNear = (await getCandles(symbol, periods.H4)).reverse()
      // List of all high points
      const high_points = [];
      // List of all low points
      const low_points = [];
      const prices = [];
      const limit = 5;

      const arrayCompareCandleCandleNear = (
        await getCandles(dataConfig.symbol, periods.D1, limit)
      ).reverse();
      // For all respones
      for (let res of Object.values(arrayCompareCandleCandleNear)) {
        // Append the information
        high_points.push(res.high);
        low_points.push(res.low);
        prices.push(res.close);
      }

      // Update the iter variables
      const maxOfRangeCandle = Math.max.apply(Math, high_points);
      const minOfRangeCandle = Math.min.apply(Math, low_points);

      if (
        compare_percentage({
          bigger: maxOfRangeCandle,
          smaller: minOfRangeCandle,
        }) >= dataConfig.config_1d_top_bottom_ratio
      ) {
        return `1D thỏa mãn điều kiện ${compare_percentage({
          bigger: maxOfRangeCandle,
          smaller: minOfRangeCandle,
        }).toFixed(2)}%`;
      }

      return "";
    } catch (e) {
      console.log(e);
    }
  },
};
