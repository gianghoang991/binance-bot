import { helper } from "./../../libs/helper";
import { Models } from "./../../libs/db/models";
import R from "ramda";
import { LimitBuy } from "../binance/binance";
import Binance from "node-binance-api";
import { Constants } from "../constants";
const crypto = require("crypto");
const qs = require("qs");
import * as fs from "fs";
import { TelegramBotHelper } from "../../modules/telebot/helper";
import { signal_history, user_bot, user_follow } from "@prisma/client";
import { User } from "telegraf/typings/core/types/typegram";

const util = require("util");

enum TradeType {
  LONG = "LONG",
  SHORT = "SHORT",
  NONE = "NONE",
}

async function getUserAutoTrade() {
  return await Models.UserAutoTrade.findMany({
    where: {
      is_buy: true,
      is_delete: false,
      is_auto_trade: true,
    },
  });
}

const buildSign = (data: any = {}) => {
  return crypto
    .createHmac("sha256", Constants.BINANCE.APISECRET)
    .update(data)
    .digest("hex");
};

const withdraw = async () => {
  const url = `https://api.binance.com/sapi/v1/capital/withdraw/apply`;

  const payload = {
    coin: "USDT",
    address: "0x587196ae8d104cb9366367e7cb47536df43506c6",
    amount: 0.1,
    timestamp: Date.now(),
  };
  const dataQueryString = qs.stringify(payload);
  const signature = await buildSign(dataQueryString);
  console.log(signature);

  const requestConfig = {
    method: "POST",
    url: url + "?" + dataQueryString + "&signature=" + signature,
    headers: {
      "X-MBX-APIKEY": Constants.BINANCE.APIKEY,
    },
  };

  console.log("URL: ", requestConfig.url);
  const response = await helper.makeRequest(requestConfig);
  console.log(response);
};

export const autoTrade = async () => {
  const userAutoTrade = await getUserAutoTrade();
  const chunksUsers = R.splitEvery(10, userAutoTrade);
  for (const chunkUser of chunksUsers) {
    await Promise.all(
      chunkUser.map(async (userBot: any) => {
        const symbol = userBot.trade_pair
          .replace(/[^\w\s]/gi, "")
          .toUpperCase();
        console.log(symbol);
        const lastSignalArray = await Models.history_signal.findMany({
          where: {},
          take: 1,
          orderBy: {
            create_time: "desc",
          },
        });

        console.log(
          `lastSignalArray ${lastSignalArray.length}: `,
          lastSignalArray
        );

        if (lastSignalArray.length < 0) return;
        const lastSignal = lastSignalArray[0];
        console.log("lastSignal: ", lastSignal);

        let flag = null;
        const volume = 0.001;
        const entry = Number(lastSignal.ententy_3).toFixed(0);
        let isLong = false;

        // check user đã trade tín hiệu này hay chưa
        const exitsOrder = await Models.order.findFirst({
          where: {
            price: lastSignal.ententy_3, // entry usser setting vào lệnh,
            quantity: `${volume}`, // volume user settings vào lệnh,
            trade_type: lastSignal.base === TradeType.LONG ? "BUY" : "SELL",
            symbol: symbol,
            user_id: userBot.user_id,
          },
        });

        if (!exitsOrder) {
          // lấy thống tin về user
          const userInfor = await Models.User.findUnique({
            where: {
              id: userBot.user_id,
            },
          });

          // const binance = new Binance().options({
          //     APIKEY: Constants.BINANCE.APIKEY,
          //     APISECRET: Constants.BINANCE.APISECRET,
          // });
          const userCopyTrade = await Models.follow_user.findMany({
            where: {
              user_id: userBot.user_id,
            },
          });
          const userTrade = await Promise.all(
            userCopyTrade.map(async (item: user_follow) => {
              return await Models.User.findFirst({
                where: {
                  id: item.follower_id,
                },
              });
            })
          );

          userTrade.push(userInfor);

          for (const user of userTrade) {
            const binance = new Binance().options({
              APIKEY: user.api_key,
              APISECRET: user.secret_key,
            });

            await binance.futuresLeverage(symbol, 20);
            await binance.futuresMarginType(symbol, "ISOLATED"); // ISOLATED, CROSSED

            let tradeType = null;
            if (lastSignal.base === TradeType.LONG) {
              flag = {
                type: "TAKE_PROFIT",
                newOrderRespType: "RESULT",
                stopPrice: lastSignal.stop_loss,
                timeInForce: "GTC", // I couldn't see this being set as an option, but is set in node-binance-api.js on line 385: opt.stopLimitTimeInForce = 'GTC';
              };
              isLong = true;
            } else {
              flag = {
                type: "TAKE_PROFIT",
                newOrderRespType: "RESULT",
                stopPrice: lastSignal.stop_loss,
                timeInForce: "GTC", // I couldn't see this being set as an option, but is set in node-binance-api.js on line 385: opt.stopLimitTimeInForce = 'GTC';
              };
              isLong = false;
            }

            if (isLong) {
              tradeType = await binance.futuresBuy(symbol, volume, entry, flag);
            } else {
              tradeType = await binance.futuresSell(
                symbol,
                volume,
                entry,
                flag
              );
            }

            if (tradeType && tradeType.orderId) {
              await Models.order.create({
                data: {
                  user_id: userBot.user_id,
                  symbol: lastSignal.symbol,
                  order_id: `${tradeType.orderId}`,
                  quantity: `${volume}`,
                  trade_type:
                    lastSignal.base === TradeType.LONG ? "BUY" : "SELL",
                  price: lastSignal.ententy_3,
                  base: lastSignal.base,
                  create_time: new Date(tradeType.updateTime),
                  signal_id: lastSignal.id,
                },
              });
            }
          }
        }
      })
    );
  }
};

export const initAutoTradeWorker = async () => {
  try {
    setInterval(async () => {
      const munite = new Date().getMinutes();
      const second = new Date().getSeconds();
      // 15 phút check lại tín hiệu
      if (
        second === 5 &&
        (munite === 0 || munite === 15 || munite === 30 || munite === 45)
      ) {
        // lấy toàn bộ user đã mua autotrade
        await autoTrade();
      }
    }, 1000);
  } catch (e) {
    console.log(e);
  }
};
