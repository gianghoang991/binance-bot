import { helper } from "../../libs/helper";
import { TelegramBotHelper } from "../telebot/helper";
import { MyError } from "../../libs/errors";
import { Constants, periods } from "../constants";
import { BinanceType } from "./type";
import { TeleBot } from "../telebot/type";
import * as fs from "fs";
import { Models } from "../../libs/db/models";
import R from "ramda";

const Binance = require("node-binance-api");

const binance = new Binance().options({
  APIKEY: Constants.BINANCE.APIKEY,
  APISECRET: Constants.BINANCE.APISECRET,
});

export const FuturePrice = async (): Promise<string> => {
  return await binance.futuresPrices();
};

export const LimitBuy = async (): Promise<void> => {
  await binance.futuresBuy("BTCUSDT", 0.1, 8222);
};

export const LimitSell = async (): Promise<void> => {
  await binance.futuresSell("BTCUSDT", 0.5, 11111);
};

export const MarketBuy = async (): Promise<void> => {
  await binance.futuresMarketBuy("BNBUSDT", 5);
};

export const MarketSell = async (): Promise<void> => {
  await binance.futuresMarketSell("TRXUSDT", 1);
};

export const FutureChart = async (
  symbol: string,
  periods: periods
): Promise<void> => {
  binance.futuresChart(symbol, periods, console.log);
};

export const MiniTicker = async (
  symbol: string
): Promise<BinanceType.MiniTicket> => {
  return new Promise((resolve, reject) => {
    binance.futuresMiniTickerStream("BTCUSDT", (tick: any) => {
      resolve(tick);
    });
  });
};

export const PlaceMutipleOrder = async (): Promise<void> => {
  let orders = [
    {
      symbol: "BNBUSDT",
      side: "SELL",
      type: "MARKET",
      quantity: "0.05",
    },
  ];
  console.info(await binance.futuresMultipleOrders(orders));
};

export const CandlestickData = async (
  symbol: string,
  period: periods
): Promise<BinanceType.BinanceTick[]> => {
  return new Promise(async (res: any, rej: any) => {
    const startTime = Date.now() - 1000 * 60 * 60 * 24;
    console.log("startTime", new Date(startTime));
    const endTime = Date.now();
    console.log("endTime", new Date(endTime));
    await binance.candlesticks(
      symbol.toUpperCase(),
      period,
      (error: any, ticks: any[], symbol: string) => {
        const data = ticks.map((tick: any) => {
          const [
            time,
            open,
            high,
            low,
            close,
            volume,
            closeTime,
            assetVolume,
            trades,
            buyBaseVolume,
            buyAssetVolume,
            ignored,
          ] = tick;
          const tickData: BinanceType.BinanceTick = {
            time: new Date(time),
            open: Number(open),
            high: Number(high),
            low: Number(low),
            close: Number(close),
            volume: Number(volume),
            closeTime: new Date(closeTime),
            assetVolume: Number(assetVolume),
            trades: parseInt(trades),
            buyBaseVolume: Number(buyBaseVolume),
            buyAssetVolume: Number(buyAssetVolume),
            ignored: Number(ignored),
          };

          return tickData;
        });

        return res(data);
      },
      { limit: 500, startTime: startTime, endTime: endTime }
    );
  });
};
export const CandlestickWebSocket = async (
  bot: any,
  symbol: string,
  period: periods,
  botType: TeleBot.BotType
): Promise<void> => {
  // Periods: 1m,3m,5m,15m,30m,1h,2h,4h,6h,8h,12h,1d,3d,1w,1M
  setInterval(async () => {
    const munite = new Date().getMinutes();
    const second = new Date().getSeconds();
    const hour = new Date().getHours();
    if (
      bot &&
      (munite === 15 || munite === 30 || munite === 45 || munite === 0) &&
      second > 5 &&
      second < 8
    ) {
      const text = await TelegramBotHelper.getDataBuySymbol(
        period,
        bot,
        botType.chatId
      );

      if (text) {
        bot.sendMessage(botType.chatId, text);
        bot.sendMessage("-1001851416522", text);
      }
    }
    if (
      bot &&
      (hour === 0 ||
        hour === 4 ||
        hour === 8 ||
        hour === 12 ||
        hour === 16 ||
        hour === 20) &&
      munite === 0 &&
      second === 0
    ) {
      const text = await TelegramBotHelper.botH4Config(symbol, periods.H4);
      bot.sendMessage(botType.chatId, text);
    }
    if (bot && hour === 0 && munite === 0 && second === 0) {
      const text = await TelegramBotHelper.bot1DConfig(symbol, periods.D1);
      bot.sendMessage(botType.chatId, text);
    }
  }, 1000);
};

// export const LastPriceWebSocket = async (): Promise<void> => {
//   binance.futuresMarkPriceStream("BTCUSDT", async (tick: any) => {
//     try {
//       const getAllOrderNotCancel = await Models.order.findMany({
//         where: {
//           is_cancel: false,
//         },
//       });
//       const chunkOrders = R.splitEvery(
//         getAllOrderNotCancel.length || 100,
//         getAllOrderNotCancel
//       );
//       for (const orders of chunkOrders) {
//         Promise.all(
//           orders.map(async (order) => {
//             const signal = await Models.history_signal.findFirst({
//               where: {
//                 id: order.signal_id,
//               },
//             });
//             if (
//               (signal.base === "LONG" && tick.markPrice <= signal.stop_loss) ||
//               (signal.base === "SHORT" && tick.markPrice >= signal.stop_loss) ||
//               (signal.base === "LONG" && tick.markPrice >= signal.tp2) ||
//               (signal.base === "SHORT" && tick.markPrice <= signal.tp2)
//             ) {
//               console.log(order.order_id);
//               const res = await binance.futuresCancel(signal.symbol, {
//                 orderId: order.order_id,
//               });
//               console.log(res);
//               await Models.order.updateMany({
//                 where: {
//                   order_id: order.order_id,
//                   user_id: order.user_id,
//                 },
//                 data: {
//                   is_cancel: true,
//                 },
//               });
//             }
//           })
//         );
//       }
//     } catch (e) {
//       console.log(e.message);
//     }
//   });
// };
