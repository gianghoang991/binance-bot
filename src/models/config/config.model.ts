export class ConfigModel {
  TOP_BOTTON_RATIO?: number;
  TP1?: number;
  TP2?: number;
  TP3?: number;
  TP4?: number;
  TP5?: number;
  TP6?: number;
  TP7?: number;
  TP8?: number;
  TP9?: number;
  TP10?: number;
  TP11?: number;
  SL_RATIO?: number;
  SL_TOP?: number;
  SL_BOTTON?: number;
  ENTRY1?: number;
  ENTRY2?: number;
  ENTRY3?: number;
  MAX_RATIO?: number;
  MIN_RATIO?: number;
  REBOUND_FACTOR?: number;
  MAX?: number;
  MIN?: number;
}
